package movies.importer;

import java.util.*;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */

//copied some code from Normalizer
public class Deduper extends Processor {
	
	public Deduper(String source,String destination) {
		super(source,destination,false);
	}
	
	public ArrayList<String> process(ArrayList<String> arrlist){
		ArrayList<Movie> movieList=new ArrayList<Movie>();
		
		for(String s: arrlist) {
			String[] kagglearr=s.split("\t");
			
			//20 is the year, 15 is the movie title, 13 is the runtime
			String year=kagglearr[0];
			String title=kagglearr[1];
			String runtime=kagglearr[2];
			
			Movie m=new Movie(year,title,runtime,"kaggle");
			movieList.add(m);
		}
		
		ArrayList<Movie> newmovieList=new ArrayList<Movie>();
		
		for(Movie m: movieList) {
			if(newmovieList.contains(m)) {
				System.out.println("contains "+m.toString());
				int index=newmovieList.indexOf(m);
				
				String source=m.getsource();
				
				if(movieList.get(index).getsource()==m.getsource()) {
					source=source+";"+newmovieList.get(index).getsource();
				}
				
				Movie newM=new Movie(m.getreleaseYear(),m.getname(),m.getruntime(),source);
				newmovieList.set(index, newM);							
			}
			else {
				newmovieList.add(m);
			}
		}
		
		ArrayList<String> StringList=new ArrayList<String>();
		for(int i=0;i<newmovieList.size();i++) {
			StringList.add(newmovieList.get(i).toString());
		}
		return StringList;
	}
}
