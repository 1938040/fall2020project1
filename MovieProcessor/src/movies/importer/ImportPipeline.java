package movies.importer;

import java.io.IOException;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
public class ImportPipeline {

	public static void main(String[] args) throws IOException {
		KaggleImporter kaggle=new KaggleImporter("C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\source","C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\KaggleImport");
		Normalizer normalized=new Normalizer("C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\KaggleImport","C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\Normaliser");
		Deduper dedupe=new Deduper("C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\Normaliser","C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\Dedupler");
		
		Processor[] procarr=new Processor[]{kaggle,normalized,dedupe};
		
		processAll(procarr);
	}
	public static void processAll(Processor[] procarr) throws IOException{
		for(Processor i:procarr) {
			i.execute();
		}
	}

}
