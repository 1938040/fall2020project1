package movies.importer;
import java.util.*;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
public class KaggleImporter extends Processor{
	
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,true);
	}
	
	public ArrayList<String> process(ArrayList<String> arrlist){
		ArrayList<String> movieList=new ArrayList<String>();

		for(String s: arrlist) {
			String[] kagglearr=s.split("\t");
			
			//20 is the year, 15 is the movie title, 13 is the runtime
			Movie m=new Movie(kagglearr[20],kagglearr[15],kagglearr[13],"kaggle");
			movieList.add(m.toString());
		}
		return movieList;
	}
}
