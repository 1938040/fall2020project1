package movies.importer;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	
	public Movie(String releaseYear,String name,String runtime,String source) {
		this.releaseYear=releaseYear;
		this.name=name;
		this.runtime=runtime;
		this.source=source;
	}
	
	public String getreleaseYear() {
		return releaseYear;
	}
	public String getname() {
		return name;
	}
	public String getruntime() {
		return runtime;
	}
	public String getsource() {
		return source;
	}

	public String toString() {
		return releaseYear+"	"+name+"	"+runtime+"	"+source;
	}
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof Movie)) { return false; }
		Movie m=(Movie)o;
		
		int max=Integer.parseInt(m.getruntime())+5;
		int min=Integer.parseInt(m.getruntime())-5;
		int time=Integer.parseInt(this.runtime);
		
		if(releaseYear.equals(m.getreleaseYear())&&name.equals(m.getname())&&time>=min&&time<=max) {
			return true;
		}
		else {
			return false;
		}
	}


	@Override
	public int hashCode() {

		return Integer.parseInt(name)+Integer.parseInt(releaseYear)+Integer.parseInt(runtime);
	}
}
