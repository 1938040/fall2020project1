package movies.importer;

import java.util.*;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
public class Normalizer extends Processor {
	
	public Normalizer(String source,String destination) {
		super(source,destination,false);
	}
	
	//copied some code from KaggleImporter
	public ArrayList<String> process(ArrayList<String> arrlist){
		ArrayList<String> movieList=new ArrayList<String>();

		for(String s: arrlist) {
			String[] kagglearr=s.split("\t");
			
			//20 is the year, 15 is the movie title, 13 is the runtime
			String year=kagglearr[0];
			String title=kagglearr[1].toLowerCase();
			String runtime=kagglearr[2];
			
			String[] split=runtime.split(" ");
			runtime=split[0];
			
			Movie m=new Movie(year,title,runtime,"kaggle");
			movieList.add(m.toString());
		}
		return movieList;
	}
}
