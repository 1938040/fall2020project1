package movies.tests;
import movies.importer.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
class KaggleImporterTest {
	public String kaggle1="Cast 1	Cast 2	Cast 3	Cast 4	Cast 5	Cast 6	Description	Director 1	Director 2	Director 3	Genre	Rating	Release Date	Runtime	Studio	Title	Writer 1	Writer 2	Writer 3	Writer 4	Year";
	public String created="0	1	2	3	4	5	6	7	8	9	10	11	12	10 minutes	14	In 10	16	17	18	19	2010";
	public String created2="0	1	2	3	4	5	6	7	8	9	10	11	12	200 Minutes	14	KaggleLand Walking and Flying2	16	17	18	19	2020";	
	
	public ArrayList<String> testList=new ArrayList<String>();
	
	@Test
	void processMethodTest() {
		String src="C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\test1kaggle\\src";
		String dest="C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\test1kaggle\\dest";
		
		testList.add(kaggle1);
		testList.add(created);
		testList.add(created2);
		
		KaggleImporter a=new KaggleImporter(src,dest);
		ArrayList<String> result=a.process(testList);
		
		String answer="Year	Title	Runtime	kaggle";
		String answer2="2010	In 10	10 minutes	kaggle";
		String answer3="2020	KaggleLand Walking and Flying2	200 Minutes	kaggle";
		
		assertEquals(answer,result.get(0));
		assertEquals(answer2,result.get(1));
		assertEquals(answer3,result.get(2));
	}

}
