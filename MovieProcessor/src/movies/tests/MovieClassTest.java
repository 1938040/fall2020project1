package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
class MovieClassTest {
	
	public String release1="2000";
	public String name1="Movie2000";
	public String runtime1="200minutes";
	public String source1="2000.com";
	public Movie a=new movies.importer.Movie(release1,name1,runtime1,source1);

	@Test
	void testgetreleaseYear() {		
		assertEquals(release1, a.getreleaseYear());
	}
	@Test
	void testgetname() {		
		assertEquals(name1, a.getname());
	}
	@Test
	void testgetruntime() {		
		assertEquals(runtime1, a.getruntime());
	}
	@Test
	void testgetsource() {		
		assertEquals(source1, a.getsource());
	}
	@Test
	void testtoString() {		
		assertEquals(release1+"	"+name1+"	"+runtime1+"	"+source1,a.toString());	
	}
}
