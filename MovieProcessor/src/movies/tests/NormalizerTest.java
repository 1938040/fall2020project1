package movies.tests;
import movies.importer.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * 
 * @author Jamin Huang 1938040
 *
 */
//copied some code from KaggleImporterTest
class NormalizerTest {
	String create1="Year	Title	Runtime	kaggle";
	String create2="2010	In 10	10 minutes	kaggle";
	String create3="2020	KaggleLand Walking and Flying2	200 Minutes	kaggle";
	
	public ArrayList<String> testList=new ArrayList<String>();
	
	@Test
	void processMethodTest() {
		String src="C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\test1kaggle\\src";
		String dest="C:\\Users\\CSA\\Documents\\dawson\\java310\\fall2020project1\\test1kaggle\\dest";
		
		testList.add(create1);
		testList.add(create2);
		testList.add(create3);
		
		Normalizer a=new Normalizer(src,dest);
		ArrayList<String> result=a.process(testList);
		
		String answer="Year	title	Runtime	kaggle";
		String answer2="2010	in 10	10	kaggle";
		String answer3="2020	kaggleland walking and flying2	200	kaggle";
		
		assertEquals(answer,result.get(0));
		assertEquals(answer2,result.get(1));
		assertEquals(answer3,result.get(2));
	}

}
